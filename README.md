# Quick Reader

A tool for quick reading.

![view](doc/speedreader.gif)

The script uses tkinter, so it needs to be installed.
```
pip3 install tkinter
```

Then, just type:
```
python3 speed_reader.py
```
