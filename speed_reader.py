#!/usr/bin/python3
# coding: utf8
###############
# Vif Lecteur #
###############

import tkinter as tk
import tkinter.filedialog as tkf
import re

class Reader():
    """
    Classe du reader
    """
    def __init__(self):
        self.tk = tk.Tk()
        self.initialize()
        self.tk.mainloop()

    def initialize(self):
        """
        Initialisation de la GUI
        """
        # Variables
        self.pause = 1
        self.position = 0
        self.vitesse = 500
        self.text = re.split(' |\n',"Ceci est un programme de lecture rapide. Ouvrez un texte dans le menu.")

        self.tk.title('Vif Lecteur')
        self.tk.grid()

        # Menu
        self.menu_bar = tk.Menu(self.tk)

        self.file_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.file_menu.add_command(label="Ouvrir", command=self.open_event)
        self.file_menu.add_command(label="Reset", command=self.reset_event)
        self.file_menu.add_command(label="Quitter", command=self.quit_event)
        self.menu_bar.add_cascade(label="Fichier", menu=self.file_menu)

        self.options_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.options_menu.add_command(label="Pause", command=self.pause_event)
        self.options_menu.add_command(label="Configurations", command=self.configure_event)
        self.menu_bar.add_cascade(label="Options", menu=self.options_menu)

        self.tk.config(menu=self.menu_bar)

        # Zone de texte
        self.text_label = tk.Label(self.tk,height=5,width=30,bg="#002B36",fg="#93A1A1",font=("Monospace", 16,))
        self.text_label.grid(row=0,column=0,columnspan=3)

        # Barre de statut
        self.status = tk.StringVar()
        self.status_bar = tk.Label(self.tk,textvariable=self.status)
        self.status_bar.grid(column=1,row=1,sticky="W")

        self.tk.grid_columnconfigure(1,weight=1)
        self.tk.resizable(True,False)
        self.tk.update()
        self.tk.geometry(self.tk.geometry())
        self.text_label.focus_set()

        # Gestion des évènements
        self.tk.after(1, self.update, self.tk)
        self.tk.bind('<Control-q>', self.quit_event)
        self.tk.bind('<Escape>', self.quit_event)
        self.tk.bind('<KeyPress-p>', self.pause_event)
        self.tk.bind('<space>', self.pause_event)
        self.tk.bind('<Left>', self.left_event)
        self.tk.bind('<Right>', self.right_event)
        self.tk.bind('<KeyPress-plus>', self.plus_event)
        self.tk.bind('<KeyPress-minus>', self.minus_event)

    def plus_event(self,event=None):
        """
        Évènement pour accélérer le texte
        """
        vitesse = 60 * 1000 // self.vitesse
        vitesse += 10
        self.vitesse = 60 * 1000 // vitesse
        self.update_status_bar()

    def minus_event(self,event=None):
        """
        Évènement pour ralentir le texte
        """
        vitesse = 60 * 1000 // self.vitesse
        if vitesse > 11:
            vitesse -= 10
        self.vitesse = 60 * 1000 // vitesse
        self.update_status_bar()

    def left_event(self,event=None):
        """
        Évènement de déplacement dans le texte
        """
        self.pause = 1

        if self.position > 0:
            self.position -= 1
        # Et on actualise le graphe
        self.update_graph()

    def right_event(self,event=None):
        """
        Évènement de déplacement dans le texte
        """
        self.pause = 1

        if self.position < len(self.text)-1:
            self.position += 1
        # Et on actualise le graphe
        self.update_graph()

    def pause_event(self,event=None):
        """
        Évènement de mise en pause
        """
        self.pause = 1-self.pause
        self.update_status_bar()
        return "break"

    def open_event(self,event=None):
        """
        Évènement pour ouvrir un fichier
        """
        filename = tkf.askopenfilename(title="Ouvrir un texte",filetypes=[('text files','.txt'),('all files','.*')])
        with open(filename, "r", encoding="utf-8") as fichier:
            self.position = 0
            contenu = fichier.read()
        contenu = contenu.replace(' ;',' ;')
        contenu = contenu.replace(' :',' :')
        contenu = contenu.replace(' !',' !')
        contenu = contenu.replace(' ?',' ?')
        contenu = contenu.replace('— ','— ')
        contenu = contenu.replace('« ','« ')
        contenu = contenu.replace(' »',' »')
        self.text = re.split(' |\n',contenu)
        self.update_graph()

    def reset_event(self,event=None):
        """
        Évènement pour revenir au début du texte
        """
        self.position = 0
        self.pause = 1
        self.update_graph()

    def quit_event(self,event=None):
        """
        Évènement pour quitter
        """
        self.tk.quit()

    def configure_event(self,event=None):
        """
        Popup de configuration
        """
        self.tk.config_toplevel = tk.Toplevel(self.tk)
        self.tk.config_toplevel.title = "Configuration"
        self.tk.config_toplevel.grid()

        # Vitesse
        vitesse_label = tk.Label(self.tk.config_toplevel,text="Vitesse :")
        vitesse_label.grid(row=0,column=0)
        self.tk.config_toplevel.vitesse_entry = tk.Entry(self.tk.config_toplevel)
        self.tk.config_toplevel.vitesse_entry.grid(row=0,column=1)
        self.tk.config_toplevel.vitesse_entry.insert(tk.END, str(60 * 1000 // self.vitesse))

        # Position
        position_label = tk.Label(self.tk.config_toplevel,text="Position :")
        position_label.grid(row=1,column=0)
        self.tk.config_toplevel.position_entry = tk.Entry(self.tk.config_toplevel)
        self.tk.config_toplevel.position_entry.grid(row=1,column=1)
        self.tk.config_toplevel.position_entry.insert(tk.END, str(self.position))

        button = tk.Button(self.tk.config_toplevel, text="OK", command=self.ferme_config)
        button.grid(row=2,column=0,columnspan=2)

    def ferme_config(self,event=None):
        """
        Validation et fermeture de la configuration
        """
        try:
            entree = int(self.tk.config_toplevel.vitesse_entry.get())
            if entree > 0:
                self.vitesse = 60 * 1000 // entree
        except:
            self.vitesse = 1000
        try:
            entree = int(self.tk.config_toplevel.position_entry.get())
            if entree >= 0 and entree < len(self.text)-1:
                self.position = entree
        except:
            self.position = 0
        self.tk.config_toplevel.destroy()

    def update_status_bar(self):
        """
        Mise à jour de la barre des statuts
        """
        if self.pause == 1:
            pause_str = "Pause"
        else:
            pause_str = "Lecture"
        vitesse_str = 60 * 1000 // self.vitesse
        pourcent_str = self.position * 100 // (len(self.text)-1)
        self.status.set("{:<8} | {:>5} mots / min. | {:>3} % ({})".format(pause_str,vitesse_str,pourcent_str,str(self.position)))

    def update_graph(self):
        # Mise à jour du texte
        self.text_label["text"] = self.text[self.position]
        # Mise à jour de la barre de statut
        self.update_status_bar()

    def update(self,event=None):
        """
        Mise à jour périodique
        """
        self.update_graph()
        # Gestion de l'avancement
        if self.pause == 0:
            if not(self.position == len(self.text)-1):
                self.position += 1
        # Again
        self.tk.after(self.vitesse, self.update, self)

if __name__ == "__main__":
    root = Reader()
